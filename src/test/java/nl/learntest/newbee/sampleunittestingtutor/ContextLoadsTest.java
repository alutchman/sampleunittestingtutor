package nl.learntest.newbee.sampleunittestingtutor;

import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.controllers.BaseErrorRestcontroller;
import nl.learntest.newbee.sampleunittestingtutor.data.repo.ZipCodeRepo;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.DuplicateItemResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.ItemNotFoundResponse;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ContextLoadsTest {
    private static String initialSessionID;

    private static RepositoryRestResource ZIPCODE_ANOTATION =
            ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
    private static final String BASE_URL = "http://localhost:%d/%s";
    private static String TEST_ZIP_CODE = "3000AN";
    private static HttpHeaders headers = new HttpHeaders();

    @Autowired
    private ApplicationContext context;

    @Value("${server.port}")
    private int port;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup(){
        if (initialSessionID == null) {
            headers.add("Accept", "application/json");
            headers.add("Content-Type", "application/json");
            headers.add("user-agent", "Mors Zilla Groom");

            String uri = String.format(BASE_URL,port,"");
            log.info("Base URL: {}", uri);

            final TestRestTemplate restTemplate = new TestRestTemplate();
            HttpEntity<String> entity = new HttpEntity<String>(null, headers);
            ResponseEntity<LinkedHashMap> response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET, entity, LinkedHashMap.class);

            List<String> coockiesInit = response.getHeaders().get("Set-Cookie");
            //headers.put(HttpHeaders.COOKIE, coockiesInit);

            String[] cookieVals = coockiesInit.get(0).split("; ");
            initialSessionID =cookieVals[0].replaceAll("JSESSIONID=","");
            headers.add(HttpHeaders.COOKIE, "JSESSIONID=" +initialSessionID);
            log.info("Initial sessionId = {} ", initialSessionID);
        }
    }


    @Test
    public void test00_ContextLoading(){
        log.info("Context loading succesful using port {}",port);
    }

    public static void SEND_TO_LOG(String key, HttpHeaders reHeaders ){
        log.info("header[{}]= {}", key, reHeaders.get(key));
    }

    @Test
    public  void test01_IndexControlerHappyFlow(){

        String uri = String.format(BASE_URL,port,"");
        log.info("Base URL: {}", uri);

        final TestRestTemplate restTemplate = new TestRestTemplate();

        HttpEntity<Void> entity = new HttpEntity<>( headers);
        ResponseEntity<LinkedHashMap> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, LinkedHashMap.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        LinkedHashMap<String,Object> respValue = response.getBody();
        Map<String, String> responseRequest = (Map<String, String>) respValue.get("request");
        assertNotNull(responseRequest);
        assertEquals(initialSessionID, responseRequest.get("sessionId"));

        log.info("headerUserAgent = {}", responseRequest.get("headerUserAgent"));
        log.info("sessionId = {}",responseRequest.get("sessionId"));
    }

    @Test
    public void test02_GetErrorPath() {
        BaseErrorRestcontroller baseErrorRestcontroller = context.getBean(BaseErrorRestcontroller.class);
        assertEquals("/error", baseErrorRestcontroller.getErrorPath());
    }

    @Test
    public  void test03_ZipCodeItemNotFound(){
        String uri = String.format(BASE_URL, port, "zipcode/"+TEST_ZIP_CODE);
        log.info("Base URL: {}", uri);

        final TestRestTemplate restTemplate = new TestRestTemplate();

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ItemNotFoundResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, ItemNotFoundResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ItemNotFoundResponse itemNotFoundResponse = response.getBody();
        assertNotNull(itemNotFoundResponse);
        assertEquals(ZIPCODE_ANOTATION.path(),itemNotFoundResponse.getEntity());
        assertEquals(TEST_ZIP_CODE,itemNotFoundResponse.getId());
        assertEquals("Zipcode "+TEST_ZIP_CODE+" does not exist",itemNotFoundResponse.getMessage());
    }


    @Test
    public  void test05_Duplicate(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE+"/Gouda/Dubbelstraat/1-10");
        log.info("Base URL: {}", uri);

        final TestRestTemplate restTemplate = new TestRestTemplate();

        //initial addition
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

        //now force duplicate error by re-adding te same values
        ResponseEntity<DuplicateItemResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.POST, entity, DuplicateItemResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        DuplicateItemResponse duplicateItemResponse = response.getBody();

        assertNotNull(duplicateItemResponse);
        assertEquals("Zipcode "+TEST_ZIP_CODE +" already exists", duplicateItemResponse.getMessage());
        assertEquals(ZIPCODE_ANOTATION.path(),duplicateItemResponse.getEntity());
        assertEquals(TEST_ZIP_CODE, duplicateItemResponse.getId());
    }

    @Test
    public void test06_closeApplication() {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("The ApplicationContext loaded for ");

        ((ConfigurableApplicationContext) context).close();
        context = null;
    }
}
