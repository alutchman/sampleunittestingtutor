package nl.learntest.newbee.sampleunittestingtutor.data.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.*;

@Slf4j
public class EncryptionDataHandlerTest {
    private static String CIPHER_ARG  = "RSA";
    private static String SIGN_ARG = "SHA512withRSA";

    private static EncryptionDataHandler DATA_HANDLER;

    @BeforeClass
    public static void beforeAllTests(){
        try {
            DATA_HANDLER = new EncryptionDataHandler();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void beforeEachTest(){
        ReflectionTestUtils.setField(DATA_HANDLER, "SIGN_ARG", SIGN_ARG);
        ReflectionTestUtils.setField(DATA_HANDLER, "CIPHER_ARG", CIPHER_ARG);
    }


    @Test
    public void encryptByKeyAsStringHappyFlow() {
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptByKeyAsString(testData, true);
        String encryptedData1 = DATA_HANDLER.encryptByKeyAsString(testData, false);
        assertNotNull(encryptedData0);
        assertNotNull(encryptedData1);
        String decryptedData0 =  DATA_HANDLER.decryptByKeyAsString(encryptedData0, false);
        String decryptedData1 =  DATA_HANDLER.decryptByKeyAsString(encryptedData1, true);
        assertNotNull(decryptedData0);
        assertNotNull(decryptedData1);
        assertEquals(testData, decryptedData0);
        assertEquals(testData, decryptedData1);
    }

    @Test
    public void decryptByKeyAsStringUnHappyFlow() {
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptByKeyAsString(testData, true);
        String encryptedData1 = DATA_HANDLER.encryptByKeyAsString(testData, false);
        assertNotNull(encryptedData0);
        assertNotNull(encryptedData1);
        String decryptedData0 =  DATA_HANDLER.decryptByKeyAsString(encryptedData1, false);
        String decryptedData1 =  DATA_HANDLER.decryptByKeyAsString(encryptedData0, true);
        assertNull(decryptedData0);
        assertNull(decryptedData1);
    }


    @Test
    public void encryptByKeyHappyFlow() {
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptData(testData, true);
        String encryptedData1 = DATA_HANDLER.encryptData(testData, false);
        assertNotNull(encryptedData0);
        assertNotNull(encryptedData1);
        String decryptedData0 =  DATA_HANDLER.decryptData(encryptedData0, false);
        String decryptedData1 =  DATA_HANDLER.decryptData(encryptedData1, true);
        assertNotNull(decryptedData0);
        assertNotNull(decryptedData1);
        assertEquals(testData, decryptedData0);
        assertEquals(testData, decryptedData1);

    }

    @Test
    public void decryptByKeyUnHappyFlow() {
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptData(testData, true);
        String encryptedData1 = DATA_HANDLER.encryptData(testData, false);
        assertNotNull(encryptedData0);
        assertNotNull(encryptedData1);
        String decryptedData0 =  DATA_HANDLER.decryptData(encryptedData1, false);
        String decryptedData1 =  DATA_HANDLER.decryptData(encryptedData0, true);
        assertNull(decryptedData0);
        assertNull(decryptedData1);
    }


    @Test
    public void decryptUsingExternalKeyTest() {
        String testData = "Dit is een test voor encryptie External";
        String publicKeyTxt = DATA_HANDLER.getPublicKeyTxt();

        String encryptedData0 = DATA_HANDLER.encryptData(testData, false);
        assertNotNull(encryptedData0);
        String decryptedData0 =  DATA_HANDLER.decryptUsingExternalKey(encryptedData0, publicKeyTxt);
        assertNotNull(decryptedData0);
        assertEquals(testData, decryptedData0);
    }

    @Test
    public void decryptUsingExternalKeyUnHappyFlowTest() {
        String testData = "Dit is een test voor encryptie External";
        String publicKeyTxt = DATA_HANDLER.getPublicKeyTxt();

        String encryptedData0 = DATA_HANDLER.encryptData(testData, true);
        assertNotNull(encryptedData0);
        String decryptedData0 =  DATA_HANDLER.decryptUsingExternalKey(encryptedData0, publicKeyTxt);
        assertNull(decryptedData0);
    }

    @Test
    public void signAndVerifyHappyFlow() {
        String data2Sing = "De tekst wordt ondertekend";
        String signedData = DATA_HANDLER.sign(data2Sing);
        assertNotNull(signedData);
        boolean verified =  DATA_HANDLER.verify(data2Sing, signedData);
        assertTrue(verified);
    }


    @Test
    public void signUnHappyFlow() {
        ReflectionTestUtils.setField(DATA_HANDLER, "SIGN_ARG", "OngeldigeCipherData");
        String data2Sing = "De tekst wordt ondertekend";
        String signedData = DATA_HANDLER.sign(data2Sing);
        assertNull(signedData);
    }

    @Test
    public void verifyUnHappyFlow() {
        String data2Sing = "De tekst wordt ondertekend";
        String signedData = DATA_HANDLER.sign(data2Sing+"xx");
        assertNotNull(signedData);
        ReflectionTestUtils.setField(DATA_HANDLER, "SIGN_ARG", "OngeldigeCipherData");
        boolean verified =  DATA_HANDLER.verify(data2Sing, signedData);
        assertFalse(verified);
    }

    @Test
    public void encryptByKeyAsStringUnHappyFlow() {
        ReflectionTestUtils.setField(DATA_HANDLER, "CIPHER_ARG", "OngeldigeCipherData");
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptByKeyAsString(testData, true);
        assertNull(encryptedData0);
    }

    @Test
    public void encryptByKeyUnHappyFlow() {
        ReflectionTestUtils.setField(DATA_HANDLER, "CIPHER_ARG", "OngeldigeCipherData");
        String testData = "Dit is een test voor encryptie";
        String encryptedData0 = DATA_HANDLER.encryptData(testData, true);
        assertNull(encryptedData0);
    }

    @Test
    public void testRemote(){
        String pubKeyTxt = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCDa7mIEqBwmm3opNmljuzoI1+O0OQ8ASLDVF0tujrgx/IG550JNTm2eJf1Qi6J4tZZzF/t80zSiZLlGPgho12pSgozrbTcxlcvpaYuX6ZY3gZPz4US69Tog0KnpqQzuUof9SW8MD6BVDORGfxj5wEUy79GtvxXl9yTePinGVbN8QIDAQAB";
        String signature = "YAzQiuSyk0/pMQj/RQ5tETEgrsp/a35Hyw+3XKaHNEexfowi6V0L8CMZH343ZNaO7U8cxphiUw1P40t901qBLI34TLRLFdcL7FLpB5ZJGKF0k421YR6CS7tycZVzNH/ndbio0n6E+O+DoXuj+dk1peHz+hW4c1EqGmNdVRq43oQ=";
        String plainText = "test of het werkt";
        try {
            byte[] publicBytes = Base64.decodeBase64(pubKeyTxt);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(keySpec);

            Signature publicSignature = Signature.getInstance(SIGN_ARG);
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes(UTF_8));
            byte[] signatureBytes = Base64.decodeBase64(signature);
            boolean verified = publicSignature.verify(signatureBytes);
            log.info("verified = {}",verified);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}