package nl.learntest.newbee.sampleunittestingtutor.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.SimpleDateFormat;

@Slf4j
public class JsonConvertUtility {
    private static final String SDF_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final JsonConvertUtility JSON_TEST_UTILITY = new JsonConvertUtility();

    private final SimpleDateFormat dateFormat = new SimpleDateFormat(SDF_FORMAT_PATTERN);
    private final ObjectMapper objectMapper = new ObjectMapper();

    private JsonConvertUtility(){
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.setDateFormat(dateFormat);
    }

    private String findJsonAsString(Object jsonTransferData){
        try {
            return objectMapper.writeValueAsString(jsonTransferData);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private <E> E findJsonObject(String jsonTransferData, Class<E> clazz){
        try {
            return objectMapper.readValue(jsonTransferData, clazz);
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static String getJsonAsString(Object jsonTransferData){
        return JSON_TEST_UTILITY.findJsonAsString(jsonTransferData);

    }


    public static <E> E getJsonObject(String jsonTransferData, Class<E> clazz){
        return JSON_TEST_UTILITY.findJsonObject(jsonTransferData, clazz);
    }




}
