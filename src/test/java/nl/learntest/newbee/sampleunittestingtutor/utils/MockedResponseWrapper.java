package nl.learntest.newbee.sampleunittestingtutor.utils;

import lombok.Getter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
public class MockedResponseWrapper extends HttpServletResponseWrapper {
    private String redirectLocation;
    private Map<String,Object> headerMap = new HashMap<>();

    public MockedResponseWrapper(HttpServletResponse response) {
        super(response);
    }

    @Override
    public void sendRedirect(String location) throws IOException {
        redirectLocation = location;
    }

    @Override
    public void addHeader(String name, String value) {
        headerMap.put(name, value);
    }

    @Override
    public void addDateHeader(String name, long date) {
        headerMap.put(name, date);
    }

    @Override
    public void addIntHeader(String name, int value) {
        headerMap.put(name, value);
    }

    public Object getHeaderValue(String name){
        return headerMap.get(name);
    }

}
