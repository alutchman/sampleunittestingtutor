package nl.learntest.newbee.sampleunittestingtutor.utils;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.http.HttpHeaders;


import java.io.IOException;

@Slf4j
public class TestRequestHttpHandler {
    public static ResponseInfo clientCallRequest(Request request){
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(request).execute();
            ResponseInfo result = new ResponseInfo(response);
            if (!result.isSuccessful()) {
                log.error(result.getMessage());
            }

            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Request contructRequestPatch(String uri, Object jsonTransferData, HttpHeaders headers){
        String jsonTransferString = (jsonTransferData.getClass().equals(String.class)) ?
                (String)jsonTransferData : JsonConvertUtility.getJsonAsString(jsonTransferData);
        MediaType JSON = MediaType.parse("application/json"); //application/vnd.api+json
        RequestBody body = RequestBody.create(JSON, jsonTransferString);


        Request request = new Request.Builder()
                .url(uri)
                .patch(body) //PUT
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .addHeader(HttpHeaders.COOKIE, headers.get(HttpHeaders.COOKIE).get(0))
                .build();
        return request;
    }

    public static Request contructRequestPost(String uri, Object jsonTransferData){
        String jsonTransferString = (jsonTransferData.getClass().equals(String.class)) ?
                (String)jsonTransferData : JsonConvertUtility.getJsonAsString(jsonTransferData);

        MediaType JSON = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(JSON, jsonTransferString);


        Request request = new Request.Builder()
                .url(uri)
                .post(body) //PUT
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }


    public static Request contructRequestGet(String uri){
        Request request = new Request.Builder()
                .url(uri)
                .get()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }

    public static Request contructRequestDelete(String uri){
        Request request = new Request.Builder()
                .url(uri)
                .delete()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }

}
