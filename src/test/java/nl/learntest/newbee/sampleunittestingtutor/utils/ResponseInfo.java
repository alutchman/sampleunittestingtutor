package nl.learntest.newbee.sampleunittestingtutor.utils;

import okhttp3.Response;

import java.io.IOException;

public class ResponseInfo {
    private final int code;
    private final boolean successful;
    private final String body;
    private final String message;

    public ResponseInfo(Response response)  {
        successful= response.isSuccessful();
        code = response.code();

        if (successful) {
            String bodyTxt = null;
            try {
                bodyTxt = response.body().string();
            } catch (IOException e) {
                bodyTxt = e.getMessage();
            } finally {
                body = bodyTxt;
            }
            message = null;
        } else {
            body = null;
            message = response.message();
        }
    }

    public int getCode() {
        return code;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getBody() {
        return body;
    }

    public String getMessage() {
        return message;
    }
}
