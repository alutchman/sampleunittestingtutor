package nl.learntest.newbee.sampleunittestingtutor.services;

import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.DuplicateItemException;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.ItemNotFoundException;
import nl.learntest.newbee.sampleunittestingtutor.data.repo.ZipCodeRepo;
import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class ZipcodeServiceTest {
    @Mock
    private ZipCodeRepo zipCodeRepo;

    private ZipcodeService zipcodeService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void prepare(){
        zipcodeService = new ZipcodeService(zipCodeRepo);
    }

    @Test
    public void findItemHappyFlow() {
        String testZipcode = "3000AN";
        ZipCodeTbl zipCodeTbl = mock(ZipCodeTbl.class);
        Optional<ZipCodeTbl> optionalZipCodeTbl = Optional.of(zipCodeTbl);

        when(zipCodeRepo.findById(testZipcode)).thenReturn(optionalZipCodeTbl);
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        ItemNotFoundException itemNotFoundException = new ItemNotFoundException(testZipcode, annotation.path(),
                "Zipcode "+ testZipcode  +" does not exist");
        ZipCodeTbl zipCodeTblResult = zipcodeService.findItem(testZipcode);
        assertEquals(zipCodeTbl, zipCodeTblResult);
    }


    @Test
    public void findItemUnHappyFlow() {
        String testZipcode = "3000AN";
        expectedException.expect(ItemNotFoundException.class);
        expectedException.expectMessage("Zipcode 3000AN does not exist");

        Optional<ZipCodeTbl> optionalZipCodeTbl = Optional.empty();

        when(zipCodeRepo.findById(testZipcode)).thenReturn(optionalZipCodeTbl);
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        ItemNotFoundException itemNotFoundException = new ItemNotFoundException(testZipcode, annotation.path(),
                "Zipcode "+ testZipcode  +" does not exist");
        zipcodeService.findItem(testZipcode);
    }


    @Test
    public void addItemHappyFlow() {
        String testZipcode = "3000AN";
        String testCity = "Rotterdam";
        String street = "Nowhere street";
        String numbers = "1-10";

        ZipCodeTbl request = new ZipCodeTbl();
        request.setZipcode(testZipcode.trim());
        request.setCity(testCity);
        request.setStreet(street);
        request.setNumbers(numbers);

        when(zipCodeRepo.existsById(testZipcode)).thenReturn(false);
        when(zipCodeRepo.save(request)).thenReturn(request);

        ZipCodeTbl result = zipcodeService.addItem(testZipcode,testCity,  street, numbers);
        assertNotNull(result);
        assertEquals(request, result);
    }

    @Test
    public void addItemUnHappyFlow() {
        String testZipcode = "3000AN";

        when(zipCodeRepo.existsById(testZipcode)).thenReturn(true);
        expectedException.expect(DuplicateItemException.class);
        expectedException.expectMessage("Zipcode "+ testZipcode  +" already exists");
        zipcodeService.addItem(testZipcode,null,  null, null);
    }


    @Test
    public void updateItemHappyFlow() {
        String testZipcode = "3000AN";
        String testCity = "Rotterdam";
        String street = "Nowhere street";
        String numbers = "1-10";

        ZipCodeTbl request = new ZipCodeTbl();
        request.setZipcode(testZipcode.trim());
        request.setCity(testCity);
        request.setStreet(street);
        request.setNumbers(numbers);

        when(zipCodeRepo.existsById(testZipcode)).thenReturn(true);
        when(zipCodeRepo.save(request)).thenReturn(request);

        ZipCodeTbl result = zipcodeService.updateItem(testZipcode,testCity,  street, numbers);
        assertNotNull(result);
        assertEquals(request, result);
    }

    @Test
    public void updateItemUnHappyFlow() {
        String testZipcode = "3000AN";
        String testCity = "Rotterdam";
        String street = "Nowhere street";
        String numbers = "1-10";

        when(zipCodeRepo.existsById(testZipcode)).thenReturn(false);
        expectedException.expect(ItemNotFoundException.class);
        expectedException.expectMessage("Zipcode "+testZipcode +" does not exist");

        zipcodeService.updateItem(testZipcode,testCity,  street, numbers);

    }

    @Test
    public void deleteZipHappyFlow() {
        String testZipcode = "3000AN";
        ZipCodeTbl zipCodeTbl = mock(ZipCodeTbl.class);
        Optional<ZipCodeTbl> optionalZipCodeTbl = Optional.of(zipCodeTbl);

        when(zipCodeRepo.findById(testZipcode)).thenReturn(optionalZipCodeTbl);

        String result = zipcodeService.deleteZip(testZipcode);
        assertEquals("OK", result);
    }


    @Test
    public void deleteZipUnHappyFlow() {
        String testZipcode = "3000AN";

        expectedException.expect(ItemNotFoundException.class);
        expectedException.expectMessage("Zipcode " + testZipcode + " does not exist");
        Optional<ZipCodeTbl> optionalZipCodeTbl = Optional.empty();
        when(zipCodeRepo.findById(testZipcode)).thenReturn(optionalZipCodeTbl);
        zipcodeService.deleteZip(testZipcode);
    }
}