package nl.learntest.newbee.sampleunittestingtutor.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class DataBaseConfigurationTest {
    private String driverClassname = "org.h2.Driver";

    private String jdbcUrl = "jdbc:h2:mem:prototype_tst";

    private String jdbcUser = "sa";

    private String jdbcPassword = "";

    private String hibernateDialect = "org.hibernate.dialect.H2Dialect";


    private String hibernateShowSql = "false";

    private String hibernateHbm2ddlAuto = "update";

    private String hibernateGenerateStatistics = "false";


    @InjectMocks
    private DataBaseConfiguration dataBaseConfiguration;

    @Before
    public void BeforeTesting(){
        ReflectionTestUtils.setField(dataBaseConfiguration, "driverClassname", driverClassname);
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcUrl", jdbcUrl);
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcUser", jdbcUser);
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcPassword", jdbcPassword);
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateDialect", hibernateDialect);
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateShowSql", hibernateShowSql);
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateHbm2ddlAuto", hibernateHbm2ddlAuto);
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateGenerateStatistics", hibernateGenerateStatistics);
    }

    @Test
    public void getDataSource() {
        DataSource ds = dataBaseConfiguration.getDataSource();
        assertNotNull(ds);

    }

    @Test
    public void entityManagerFactory() {
        DataSource ds = mock(DataSource.class);
        LocalContainerEntityManagerFactoryBean em =  dataBaseConfiguration.entityManagerFactory(ds);
        assertNotNull(em);
        assertEquals(ds, em.getDataSource());
        String dialect = (String) em.getJpaPropertyMap().get("hibernate.dialect");
        assertEquals(hibernateDialect, dialect);
    }

    @Test
    public void transactionManager() {
        EntityManagerFactory em = mock(EntityManagerFactory.class);
        PlatformTransactionManager tm = dataBaseConfiguration.transactionManager(em);
        assertNotNull(tm);

    }

    @Test
    public void dispatcherServlet() {
    }
}