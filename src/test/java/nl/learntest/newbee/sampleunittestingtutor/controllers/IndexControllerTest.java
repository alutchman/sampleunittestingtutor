package nl.learntest.newbee.sampleunittestingtutor.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.services.AuthenticationService;
import nl.learntest.newbee.sampleunittestingtutor.utils.MockedResponseWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class IndexControllerTest {
    public static final String SomeSessionID = "2314234234234gdfgdfgsfgsdgfdg";
    private static final String SOME_REMOTE_HOST = "https://www.nowhere.com" ;
    private static final String SOME_USER_AGENT = "Most Zilla" ;
    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCV" +
            "J79A0Be2X863gu2BjFCbwv/DbF+iK6/Prpm15M8zGDJgyoa+iBeJ/F+b5MHhHS50jV3ydLY" +
            "VOys5FF3H36EfgxuOyVzrBK+UCeJ3UcRFh+kL1WZh1hXnRoADDcQvbaF3+j+PQMtQDc" +
            "IcmrCXuKYR//vqjFEPlmVLcCqUuBuZSwIDAQAB";
    private ObjectMapper objectMapper = mock(ObjectMapper.class);
    private AuthenticationService authenticationService = mock(AuthenticationService.class);

    public IndexController indexController;

    @Before
    public void setup(){

        indexController = new IndexController(objectMapper,authenticationService);
    }

    @Test
    public void getResources() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(session.getId()).thenReturn(SomeSessionID);
        when(request.getSession()).thenReturn(session);
        when(request.getRemoteHost()).thenReturn(SOME_REMOTE_HOST);
        when(request.getHeader("user-agent")).thenReturn(SOME_USER_AGENT);
        when(authenticationService.getPublicKey()).thenReturn(PUBLIC_KEY);

        Map<String, Object> result =  indexController.getResources(request);
        Map<String, String> responseRequest = (Map<String, String>) result.get("request");

        assertNotNull(responseRequest);
        assertEquals(SOME_USER_AGENT, responseRequest.get("headerUserAgent"));
        assertEquals(SomeSessionID, responseRequest.get("sessionId"));
        String foundPublicKey = (String ) result.get("publicKey");
        assertNotNull(foundPublicKey);
        assertEquals(PUBLIC_KEY, foundPublicKey);

        log.info(result.toString());
    }

    @Test
    public void handleErrorHtmlPost() throws IOException, ServletException {
        MockHttpSession mockHttpSession = new MockHttpSession();

        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.setSession(mockHttpSession);

        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        Date startDate = new Date();
        indexController.handleErrorHtml(mockHttpServletRequest,mockHttpServletResponse);

        assertTrue((Boolean) mockHttpServletRequest.getAttribute("invalidCredentials"));
        assertEquals("/goback", mockHttpServletResponse.getRedirectedUrl());
        Long errortime = mockHttpServletResponse.getDateHeader("errorTime");

        assertNotNull(errortime);

        long diffInMillies = Math.abs(startDate.getTime() - errortime);
        assertTrue(diffInMillies < 60000l); //less than a minute
        assertTrue((Boolean) mockHttpSession.getAttribute("invalidCredentials"));
    }
}