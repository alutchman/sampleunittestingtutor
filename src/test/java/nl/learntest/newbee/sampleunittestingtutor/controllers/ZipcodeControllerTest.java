package nl.learntest.newbee.sampleunittestingtutor.controllers;

import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.DuplicateItemException;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.ItemNotFoundException;
import nl.learntest.newbee.sampleunittestingtutor.data.repo.ZipCodeRepo;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.DuplicateItemResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.ItemNotFoundResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import nl.learntest.newbee.sampleunittestingtutor.services.ZipcodeService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class ZipcodeControllerTest {

    @Mock
    private ZipcodeService zipcodeService;

    private ZipcodeController zipcodeController;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void prepare(){
        zipcodeController = new ZipcodeController(zipcodeService);
    }

    @Test
    public void getZipcodeData() {
        String testZipcode = "3000AN";
        ZipCodeTbl zipCodeTbl = mock(ZipCodeTbl.class);
        when(zipcodeService.findItem(testZipcode)).thenReturn(zipCodeTbl);
        ZipCodeTbl zipCodeTblResult =zipcodeController.getZipcodeData(testZipcode);
        assertNotNull(zipCodeTblResult);
        assertEquals(zipCodeTbl, zipCodeTblResult);
    }


    @Test
    public void addZipcode() {
        String testZipcode = "3000AN";
        String testCity = "Rotterdam";
        String street = "Nowhere street";
        String numbers = "1-10";
        ZipCodeTbl zipCodeTbl = mock(ZipCodeTbl.class);

        when(zipcodeService.addItem(testZipcode, testCity, street, numbers)).thenReturn(zipCodeTbl);
        ZipCodeTbl zipCodeTblResult =  zipcodeController.addZipcode(testZipcode, testCity, street, numbers);
        assertEquals(zipCodeTbl, zipCodeTblResult);
    }

    @Test
    public void patchZipcode() {
        String testZipcode = "3000AN";
        String testCity = "Rotterdam";
        String street = "Nowhere street";
        String numbers = "1-10";
        zipcodeController.patchZipcode(testZipcode, testCity, street, numbers);
    }

    @Test
    public void deleteZipcode() {
        String testZipcode = "3000AN";
        zipcodeController.deleteZipcode(testZipcode);
    }

    @Test
    public void databaseError() {
        String testZipcode = "3000AN";
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        DuplicateItemException duplicateItemException =
                new DuplicateItemException(testZipcode,  annotation.path(), "Zipcode "+ testZipcode  +" already exists");

        DuplicateItemResponse result = zipcodeController.databaseError(duplicateItemException);
        assertNotNull(result);
        assertEquals(duplicateItemException.getMessage(), result.getMessage());
        assertEquals(annotation.path(), result.getEntity());
        assertEquals(testZipcode, result.getId());
    }

    @Test
    public void itemNotFound() {
        String testZipcode = "3000AN";
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        ItemNotFoundException itemNotFoundException =
                new ItemNotFoundException(testZipcode, annotation.path(), "Zipcode "+ testZipcode  +" does not exist");

        ItemNotFoundResponse itemNotFoundResponse = zipcodeController.itemNotFound(itemNotFoundException);
        assertNotNull(itemNotFoundResponse);
        assertEquals(itemNotFoundException.getMessage(), itemNotFoundResponse.getMessage());
        assertEquals(annotation.path(), itemNotFoundResponse.getEntity());
        assertEquals(testZipcode, itemNotFoundResponse.getId());
    }
}