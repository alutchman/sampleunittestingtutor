package nl.learntest.newbee.sampleunittestingtutor;

import nl.learntest.newbee.sampleunittestingtutor.config.DataBaseConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Import(DataBaseConfiguration.class)
@EnableJpaRepositories(basePackages = {
        "nl.learntest.newbee.sampleunittestingtutor.data.repo"
})
@ComponentScan({"nl.learntest.newbee.sampleunittestingtutor.controllers","nl.learntest.newbee.sampleunittestingtutor.services"})
public class SampleUnitTestingTutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleUnitTestingTutorApplication.class, args);
    }


}
