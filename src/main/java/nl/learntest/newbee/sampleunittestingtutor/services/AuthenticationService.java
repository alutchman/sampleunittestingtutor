package nl.learntest.newbee.sampleunittestingtutor.services;

import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.data.utils.EncryptionDataHandler;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationService {
    private static EncryptionDataHandler ENCRYPTION_DATA_HANDLER;

    static {
        try {
            ENCRYPTION_DATA_HANDLER = new EncryptionDataHandler();
        } catch (Exception e) {
            ENCRYPTION_DATA_HANDLER =null;
        }
    }

    public String getPublicKey(){
        return ENCRYPTION_DATA_HANDLER.getPublicKeyTxt();
    }

    public String signData(String value){
        return ENCRYPTION_DATA_HANDLER.sign(value);
    }

    public boolean verify(String value, String sign){
        return ENCRYPTION_DATA_HANDLER.verify(value, sign);
    }
}
