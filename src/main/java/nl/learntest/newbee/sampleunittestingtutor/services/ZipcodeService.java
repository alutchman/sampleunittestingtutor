package nl.learntest.newbee.sampleunittestingtutor.services;

import lombok.AllArgsConstructor;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.DuplicateItemException;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.ItemNotFoundException;
import nl.learntest.newbee.sampleunittestingtutor.data.repo.ZipCodeRepo;
import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ZipcodeService {
     private ZipCodeRepo zipCodeRepo;

    public ZipCodeTbl findItem(String zipcode) {
        Optional<ZipCodeTbl> zipOption =  zipCodeRepo.findById(zipcode.trim());
        if (zipOption.isPresent()) {
            return zipOption.get();
        }
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        throw new ItemNotFoundException(zipcode, annotation.path(), "Zipcode "+ zipcode  +" does not exist");
    }

    @Transactional
    public ZipCodeTbl addItem(String zipcode, String city, String street, String numbers) {
        if (zipCodeRepo.existsById(zipcode)) {
            RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
            throw new DuplicateItemException(zipcode,  annotation.path(), "Zipcode "+ zipcode  +" already exists");
        }

        ZipCodeTbl request = new ZipCodeTbl();
        request.setZipcode(zipcode.trim());
        request.setCity(city);
        request.setStreet(street);
        request.setNumbers(numbers);

        return zipCodeRepo.save(request);
    }

    @Transactional
    public ZipCodeTbl updateItem(String zipcode, String city, String street, String numbers) {
        if (!zipCodeRepo.existsById(zipcode)) {
            RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
            throw new ItemNotFoundException(zipcode, annotation.path(), "Zipcode "+ zipcode  +" does not exist");
        }

        ZipCodeTbl request = new ZipCodeTbl();
        request.setZipcode(zipcode.trim());
        request.setCity(city);
        request.setStreet(street);
        request.setNumbers(numbers);

        return zipCodeRepo.save(request);
    }


    @Transactional
    public String deleteZip(String zipcode) {
        Optional<ZipCodeTbl> zipOption =  zipCodeRepo.findById(zipcode.trim());
        if (zipOption.isPresent()) {
            ZipCodeTbl zipCodeTbl =   zipOption.get();
            zipCodeRepo.delete(zipCodeTbl);
            return "OK";
        }
        RepositoryRestResource annotation =  ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);
        throw new ItemNotFoundException(zipcode, annotation.path(), "Zipcode "+ zipcode  +" does not exist");
    }

}
