package nl.learntest.newbee.sampleunittestingtutor.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.BasicErrorResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.SignInfo;
import nl.learntest.newbee.sampleunittestingtutor.services.AuthenticationService;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Slf4j
@RestController
public class IndexController {
    @Autowired
    private Environment environment;

    private ObjectMapper objectMapper;

    private AuthenticationService authenticationService;

    @Autowired
    public IndexController(ObjectMapper objMapper, AuthenticationService authenticationService){
        this.objectMapper = objMapper;
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        this.objectMapper.setDateFormat(sdf);

        this.authenticationService = authenticationService;
    }

    @RequestMapping("/")
    public Map<String, Object> getResources(HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();

        Map<String, String> reqData = new HashMap<>();
        reqData.put("sessionId", request.getSession().getId());
        reqData.put("remoteHost", request.getRemoteHost());
        reqData.put("headerUserAgent", request.getHeader("user-agent"));
        result.put("request", reqData);
        String publicKey = authenticationService.getPublicKey();
        result.put("publicKey", publicKey);
        return result;
    }


    @RequestMapping("sign.txt")
    public SignInfo signData(@RequestParam("subject") String onderwerp) throws EncoderException {
        String signature = authenticationService.signData(onderwerp);
        String port = environment.getProperty("local.server.port");
        log.info("controle http://localhost:{}/check.txt?subject={}&sign={}",port,
                new URLCodec().encode(onderwerp),  new URLCodec().encode(signature));
        return new SignInfo(onderwerp, signature,"Onbekend");
    }

    @RequestMapping("check.txt")
    public SignInfo verify(@RequestParam("subject") String onderwerp,@RequestParam("sign") String signature){
        //signature = signature.replaceAll(" ", "+");
        //log.info("verify with sign = \"{}\"",signature);
        boolean verified = authenticationService.verify(onderwerp,signature );
        String status = verified ? "verified sucess" : "verified failed";
        return new SignInfo(onderwerp, signature,status);
    }

    @RequestMapping(value = "/unauth", method = RequestMethod.POST)
    public void handleErrorHtml(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String requestURL = req.getRequestURL().toString();

        req.setAttribute("invalidCredentials", true);
        log.info("---------------requestURL------------------------------- "+ requestURL);
        req.getSession().setAttribute("invalidCredentials", true);
        res.setDateHeader("errorTime", new Date().getTime());
        res.sendRedirect("/goback");  //dit komt terecht in vaadin vanwege de mapping /*
    }

    @RequestMapping(value = "/goback", method = RequestMethod.POST)
    public BasicErrorResponse handleGoBack(HttpServletRequest req, HttpServletResponse res) throws IOException {
        BasicErrorResponse basicErrorResponse = new BasicErrorResponse();
        basicErrorResponse.setTimestamp(new Date());
        basicErrorResponse.setPath("/unauth");
        basicErrorResponse.setError("Service error");
        basicErrorResponse.setStatus(500);
        basicErrorResponse.setMessage("You supplied invalid credentials");
        return basicErrorResponse;
    }

}
