package nl.learntest.newbee.sampleunittestingtutor.controllers;

import nl.learntest.newbee.sampleunittestingtutor.data.responses.BasicErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.Map;

@PropertySource("classpath:error.properties")
@RestController
public class BaseErrorRestcontroller implements ErrorController {
    private boolean debug = false;

    @Value("${error.url}")
    private String errorPath;

    @Value("${timestamp.key}")
    private String timestampKey;

    @Value("${status.key}")
    private String statusKey;

    @Value("${error.key}")
    private String errorKey;

    @Value("${message.key}")
    private String messageKey;

    @Value("${path.key}")
    private String pathKey;


    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = "${error.url}")
    public BasicErrorResponse error(WebRequest webRequest) {
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);
        BasicErrorResponse basicErrorResponse = new BasicErrorResponse();
        basicErrorResponse.setStatus((Integer) errorInfo.get(statusKey));
        basicErrorResponse.setTimestamp((Date)  errorInfo.get(timestampKey));
        basicErrorResponse.setError((String)  errorInfo.get(errorKey));
        basicErrorResponse.setMessage((String)  errorInfo.get(messageKey));
        basicErrorResponse.setPath((String)  errorInfo.get(pathKey));

        return basicErrorResponse;
    }

    @Override
    public String getErrorPath() {
        return errorPath;
    }
}

