package nl.learntest.newbee.sampleunittestingtutor.Exceptions;

import lombok.Getter;

@Getter
public class ItemNotFoundException extends RuntimeException {
    private final String id;
    private final String entity;

    public ItemNotFoundException(String id, String entity, String message){
        super(message);
        this.id = id;
        this.entity = entity;
    }
}
