package nl.learntest.newbee.sampleunittestingtutor.data.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SignInfo {
    private String message;
    private String signature;
    private String status;
}
