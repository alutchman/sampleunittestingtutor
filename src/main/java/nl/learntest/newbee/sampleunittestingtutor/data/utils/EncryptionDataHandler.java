package nl.learntest.newbee.sampleunittestingtutor.data.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
public class EncryptionDataHandler {
    private static final int KEY_LENGTH = 1024;
    private static String CIPHER_ARG  = "RSA";
    private static String SIGN_ARG = "SHA512withRSA";

    private PrivateKey privateKey;
    private PublicKey publicKey;
    private String publicKeyTxt;
    private String privateKeyTxt;

    public EncryptionDataHandler() throws NoSuchAlgorithmException{
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(KEY_LENGTH);
        KeyPair keyPair = keyPairGenerator.genKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
        publicKeyTxt = Base64.encodeBase64String(publicKey.getEncoded());
        privateKeyTxt = Base64.encodeBase64String(privateKey.getEncoded());
    }

    public String encryptByKeyAsString(String data, boolean usePublic)  {
         try {
            Cipher cipher  = Cipher.getInstance(CIPHER_ARG);
            Key keyToUse;
            if (usePublic) {
                keyToUse = KeyFactory.getInstance("RSA").
                        generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(publicKeyTxt.getBytes())));
            } else {
                keyToUse = KeyFactory.getInstance("RSA").
                        generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKeyTxt.getBytes())));
            }
            cipher.init(Cipher.ENCRYPT_MODE, keyToUse);
            byte[] encryptedbytes = cipher.doFinal(data.getBytes());
            return new String(Base64.encodeBase64String(encryptedbytes));
        } catch (Exception e) {
             return null;
        }
    }

    public String decryptUsingExternalKey(String data, String extrenalKey){
        try {
            PublicKey publicKeyToUse = KeyFactory.getInstance("RSA").
                    generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(extrenalKey.getBytes())));
            Cipher cipher = Cipher.getInstance(CIPHER_ARG);
            cipher.init(Cipher.DECRYPT_MODE, publicKeyToUse);
            byte[] encryptedbytes = cipher.doFinal(Base64.decodeBase64(data.getBytes()));
            return new String(encryptedbytes);
        } catch (Exception e) {
            return null;
        }

    }


    public String decryptByKeyAsString(String data, boolean usePublic) {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_ARG);
            Key keyToUse;
            if (usePublic) {
                keyToUse = KeyFactory.getInstance("RSA").
                        generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(publicKeyTxt.getBytes())));
            } else {
                keyToUse = KeyFactory.getInstance("RSA").
                        generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKeyTxt.getBytes())));
            }
            cipher.init(Cipher.DECRYPT_MODE, keyToUse);
            byte[] encryptedbytes = cipher.doFinal(Base64.decodeBase64(data.getBytes()));
            return new String(encryptedbytes);

        } catch (Exception e) {
            return null;
        }
    }


    public String sign(String plainText){
        try {
            Signature privateSignature = Signature.getInstance(SIGN_ARG);
            privateSignature.initSign(privateKey);
            privateSignature.update(plainText.getBytes(UTF_8));
            byte[] signature = privateSignature.sign();
            return Base64.encodeBase64String(signature);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return null;
    }

    public boolean verify(String plainText, String signature) {
        try {
            Signature publicSignature = Signature.getInstance(SIGN_ARG);
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes(UTF_8));
            byte[] signatureBytes = Base64.decodeBase64(signature);
            return publicSignature.verify(signatureBytes);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }


    public String encryptData(String dataPlain, boolean usePublic) {
        try {
            Cipher cipher  = Cipher.getInstance(CIPHER_ARG);
            cipher.init(Cipher.ENCRYPT_MODE,usePublic ? publicKey : privateKey);
            byte[] encryptedbytes = cipher.doFinal(dataPlain.getBytes());
            return new String(Base64.encodeBase64String(encryptedbytes));
        } catch (Exception e) {
            return null;
        }
    }

    public String decryptData(String dataEncrypted, boolean usePublic) {
        try {
            Cipher cipher  = Cipher.getInstance(CIPHER_ARG);
            cipher.init(Cipher.DECRYPT_MODE,usePublic ? publicKey : privateKey);
            byte[] encryptedbytes = cipher.doFinal(Base64.decodeBase64(dataEncrypted));
            return new String(encryptedbytes);
        } catch (Exception e) {
            return null;
        }
    }

    public String getPublicKeyTxt() {
        return publicKeyTxt;
    }

    public static String getSignArg() {
        return SIGN_ARG;
    }
}
