package nl.learntest.newbee.sampleunittestingtutor.data.repo;

import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "zipcodes", path = "zipcode")
public interface ZipCodeRepo extends PagingAndSortingRepository<ZipCodeTbl, String> {

}
