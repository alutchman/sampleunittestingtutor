package nl.learntest.newbee.sampleunittestingtutor.data.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ItemNotFoundResponse {
    private String id;
    private String entity;
    private String message;

}
