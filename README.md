# README #

* Spring Boot JSON Rest API
* JPA with Hibernate
* Entities require JPARespostiories and Services

# TEST Methods #
* SpringBootTest using @SpringRunner (Integration tests)
* Mockito (General Unit tests)

# Sonar #
mvn clean install -PsonarLocal